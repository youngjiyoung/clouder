#!/usr/bin/env ruby

class Client
  # client class for running a soundcloud updater
  require 'rubygems'
  require 'socket'
  require 'getoptlong'
  require 'soundcloud'
  require 'yaml'
  require 'popen4'
  require 'fileutils'
  require 'rykou'
  require 'taglib'
  require 'opus-ruby'

  # TODO: delete the server imolementation
  # TODO: imporve the outputs

  attr_accessor :verbose
  def initialize(args)
    # initlializing some values and starting the program with a token check
    @configpath = File.join(Dir.home, '.config/clouder/config.yaml')
    @config = read_config
    @client_id = '2e675c4b5bab63da046c38e57318c9b0'
    @logger = Rykou::Logging.new(verbose: args[:verbose])
    @logger.log_path :'/home/rykou/Development/clouder/client.log'
    welcome
    read_token
  end

  def welcome
    puts "Welcome to clouder, clouder is starting up."
    @logger.print "Verbose mode active"
  end

  def read_token
    # checks to see if our config already has a token stored.
    # False: authorize the program.
    # True: authenticate with the current token.
    begin
      data = Rykou::Yaml.load(@configpath, false)
      @logger.print(data)
      if data['account'].nil? then authorize else authenticate(data['account']['access_token']) end
    rescue NoMethodError => e
      #authorize
      @logger.print e.message, e.backtrace
    rescue StandardError => e
      @logger.print e.message, e.backtrace
    end
  end

  def read_config
    # read the config that is stored in our yaml.
    begin
      data = Rykou::Yaml.load(@configpath, false)
      return data['config'] unless data['config'].nil?
    rescue NoMethodError
      @logger.print "Config file is empty"
    end
  end


  def authorize
    # authorizing the program trough 2 factor authorization
    client = Soundcloud.new(client_id: @client_id,
                            client_secret: '70d8ca1b947cd503b1642d342a405161',
                            redirect_uri: 'test://test/callback')
    p client.authorize_url(scope: 'non-expiring')
    status = POpen4::popen4("xdg-open '#{client.authorize_url(scope: 'non-expiring')}'") do |stdout, stdin, stderr, pid |
      p ""
    end
  end

  def authenticate(token)
    # authenticate the user with soundcloud
    @logger.print('auth plz')
    @logger.print(token)
    @client = Soundcloud.new(access_token: token)
    @logger.print(@client)
    current_user = @client.get('/me')
    following = @client.get('/me/followings').first
    @downloaded_a_track = false
    following[1].each do |accountdata|
      @logger.print accountdata['username']
      change_directory(accountdata)
      list_tracks(accountdata)
      execute_hook unless @config['hooks'].nil?
    end
    if @downloaded_a_track
      message = "Hello Rykou,\nI have downloaded some tracks for you:\n"
      @downloaded_tracks.each do |filename|
        message += "#{filename}\n"
      end
      message += "\nThat is a total of #{@downloaded_tracks.length}.\nHave a nice day<3!"
      @logger.mail(subject: "Downloaded song of the internet for you", message: message, mail: "rykounanba@vivaldi.net")
    end
  end

  def list_tracks(profile)
    tracks = @client.get("/users/#{profile['id']}/tracks")
    tracks.each do |track|
      if track['downloadable']
        download_track(track)
      else
        download_track(track, false)
      end
    end
  end

  def download_track(trackdata, internal = true)
    # track downloader
    # for legally downlaodable tracks we download them trough the provided /
    # links from soundcloud. else we use youtube-dl to downloaad it for us.
    filename = "#{trackdata['user']['username'].delete('/')} - #{trackdata['title'].delete('/')}.#{trackdata['original_format']}"
    projected_filename = "#{trackdata['user']['username'].delete('/')} - #{filter_characters(trackdata['title'])}.opus"
    filename = filter_characters(filename)
    return if File.file? projected_filename # if file exists
    @logger.print "still downloading the file: #{filename}" , "Projected: #{projected_filename}"
    @downloaded_a_track = true
    @downloaded_tracks = [] if @downloaded_tracks.nil?
    @downloaded_tracks.push(filename)
    if internal
      resp = nil
      url = trackdata['download_url']
      Net::HTTP.start(url.gsub('https://', '').split('/')[0]) do |http|
        resp = http.get("/#{url.gsub('https://', '').split('/', 2)[1]}?client_id=#{@client_id}")
        if resp['status']
          Net::HTTP.start(resp['location'].gsub('http://', '').split('/')[0]) do |innerhttp|
            resp = innerhttp.get("/#{resp['location'].gsub('http://', '').split('/')[1]}")
          end
        end
        open(filename, 'wb') do |file|
          # actual writing of the song
          file.write(resp.body)
        end
      end
      add_meta_data(trackdata, filename)
    else
      # not internal
      status = POpen4::popen4("youtube-dl '#{trackdata['permalink_url']}' --add-metadata -o '#{filename}'") do |stdout, stdin, stderr, pid|
      end
    end
  end

  def execute_hook
    # TODO: include more hooks/ also, make these hooks more secure
    system(@config['hooks']['post-all']) unless @config['hooks']['post-all'].nil?
  end

  def filter_characters(string)
    string.delete("'").delete('?').delete('*').delete('⊕')
  end

  def add_meta_data(trackdata, file)
    # adding meta data to the downloaded song
    TagLib::FileRef.open(file) do |fileref|
      unless fileref.null?
        tag = fileref.tag
        tag.title = trackdata['title']
        tag.artist = trackdata['user']['username']
        tag.genre = trackdata['genre']
        fileref.save
      end
    end
  end

  def parse_directory(format, profile)
    return Dir.getwd if @config['directory'].nil?
    if format.include? '$DIRECTORY'
      format = format.gsub('$DIRECTORY', @config['directory'])
    end
    if format.include? '$ARTIST'
      format = format.gsub('$ARTIST', profile['username'].gsub('/',''))
    end
    format
  end

  def change_directory(profile)
    return if profile.nil?
    begin
      parse_directory('$DIRECTORY/krap/$ARTIST', profile)
      Dir.chdir(parse_directory(@config['format'], profile))
      @logger.print "current directory is: #{Dir.getwd}"
    rescue Errno::ENOENT
      create_directory(profile)
      change_directory(profile)
    end
  end

  def create_directory(profile)
    return if profile.nil?
    Dir.mkdir(parse_directory(@config['format'], profile), 0700)
  end

  def send(message)
    sock = TCPSocket.new 'localhost', 2000
    @logger.print message
    sock.puts message
    sock.close
  end
end

if __FILE__ == $PROGRAM_NAME
  #initial option values
  verbose = false

  opts = GetoptLong.new(
    ['--help', '-h', GetoptLong::NO_ARGUMENT],
    ['--send', '-s', GetoptLong::REQUIRED_ARGUMENT],
    ['--verbose', '-v', GetoptLong::NO_ARGUMENT]
  )

  opts.each do |opt, arg|
    case opt
    when '--help'
      puts '--help : print this help menu\n
            --verbose : print all debugging messages'
    when '--verbose'
     verbose = true
    when '--send'
      parsed = arg.split('#access_token=')[1].split('&')[0]
      configpath = File.join(Dir.home, '.config/clouder/config.yaml')
      config_data = Rykou::Yaml.load(configpath, false)
      log.print config_data
      if config_data['account'].nil?
        config_data['account'] = {}
        config_data['account']['access_token'] = parsed
      end
      Rykou::Yaml.save(configpath, config_data, false)
      exit
    end
  end
  client = Client.new(verbose: verbose)
end
